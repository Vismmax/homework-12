import call from 'react-native-phone-call';

const phoneCall = (number) => {
  const args = {
    number,
    prompt: false,
  };

  call(args).catch(console.error);
};

export {phoneCall};
