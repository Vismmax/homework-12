import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';

const dataContacts = [
  {
    recordID: '6b2237ee0df85980',
    familyName: 'Jung',
    givenName: 'Carl',
    emailAddresses: [
      {
        label: 'work',
        email: 'carl-jung@example.com',
      },
    ],
    phoneNumbers: [
      {
        label: 'mobile',
        number: '(555) 555-5555',
      },
    ],
    hasThumbnail: true,
    thumbnailPath:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
  },
  {
    recordID: '6b2237ee0df85981',
    familyName: 'Jong',
    givenName: 'Carlay',
    emailAddresses: [
      {
        label: 'work',
        email: 'carlay-jung@example.com',
      },
    ],
    phoneNumbers: [
      {
        label: 'mobile',
        number: '(777) 777-7777',
      },
    ],
    hasThumbnail: true,
    thumbnailPath:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
  },
  {
    recordID: '6b2237ee0df85982',
    familyName: 'Dung',
    givenName: 'Fong',
    emailAddresses: [
      {
        label: 'work',
        email: 'fong-jung@example.com',
      },
    ],
    phoneNumbers: [
      {
        label: 'mobile',
        number: '(999) 999-9999',
      },
    ],
    hasThumbnail: true,
    thumbnailPath: 'content://com.android.contacts/display_photo/3',
  },
];

export const getContacts = (cb) => {
  PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
    title: 'Contacts',
    message: 'This app would like to view your contacts.',
    buttonPositive: 'Please accept bare mortal',
  }).then(() => {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        // error
      } else {
        contacts = contacts.length ? contacts : dataContacts;
        const contactsList = contacts.map((data) => ({
          id: data.recordID,
          name: data.displayName ?? `${data.familyName} ${data.givenName}`,
          emails: data?.emailAddresses.map((e) => e.email),
          phones: data?.phoneNumbers.map((e) => e.number),
          avatar: data?.thumbnailPath || '',
        }));
        const sortList = contactsList.sort((a, b) => {
          if (a.name > b.name) {
            return 1;
          }
          if (a.name < b.name) {
            return -1;
          }
          return 0;
        });
        cb(sortList);
      }
    });
  });
};

export const openContactForm = (contact, cb) => {
  PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
    title: 'Contacts',
    message: 'This app would like to edit your contacts.',
    buttonPositive: 'Please accept bare mortal',
  }).then(() => {
    Contacts.openContactForm(contact, (err, addedContact) => {
      if (err === 'denied') {
        throw err;
      }
      cb(addedContact);
    });
  });
};

export const getContact = (id, cb) => {
  PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
    title: 'Contacts',
    message: 'This app would like to view your contacts.',
    buttonPositive: 'Please accept bare mortal',
  }).then(() => {
    Contacts.getContactById(id, (error, contact) => {
      cb(contact);
    });
  });
};

export const addContact = (cb) => {
  openContactForm({}, cb);
};

export const editContact = (id, cb) => {
  getContact(id, (contact) => {
    if (contact) {
      openContactForm(contact, (data) => {
        if (data) {
          deleteContact(contact, cb);
        }
      });
    } else {
      cb();
    }
  });
};

export const deleteContact = (contact, cb) => {
  PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
    title: 'Contacts',
    message: 'This app would like to edit your contacts.',
    buttonPositive: 'Please accept bare mortal',
  }).then(() => {
    Contacts.deleteContact(contact, (err) => {
      if (err === 'denied') {
        throw err;
      }
      cb();
    });
  });
};
