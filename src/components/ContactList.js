import React, {useEffect, useState} from 'react';
import {Icon, SearchBar} from 'react-native-elements';
import {View, StyleSheet, FlatList} from 'react-native';
import ContactItem from './ContactItem';
import {getContacts, addContact, editContact} from '../services/contactService';

const ContactList = ({route, navigation}) => {
  const [searchValue, setSearchValue] = useState('');
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    updateContacts();
  }, [route.params]);

  const openContactInfo = (contact) => {
    navigation.navigate('Details', {contact});
  };

  const updateContacts = () => {
    getContacts((data) => {
      setContacts(data);
    });
  };

  const add = () => {
    addContact(updateContacts);
  };

  const edit = (contact) => {
    editContact(contact.id, updateContacts);
  };

  const list = contacts.filter(
    (contact) =>
      contact.name.includes(searchValue) ||
      contact.phones.some((el) => el.includes(searchValue)) ||
      contact.emails.some((el) => el.includes(searchValue)),
  );

  return (
    <View style={styles.container}>
      <SearchBar
        lightTheme={true}
        placeholder="Name / Phone / Email"
        onChangeText={(value) => setSearchValue(value)}
        value={searchValue}
      />
      <FlatList
        data={list}
        renderItem={({item}) => (
          <ContactItem
            key={item.id}
            contact={item}
            onPress={openContactInfo}
            edit={edit}
          />
        )}
      />
      <View>
        <Icon
          name="add"
          color="green"
          reverse
          raised
          containerStyle={styles.btnAdd}
          onPress={add}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnAdd: {
    position: 'absolute',
    right: 10,
    bottom: 10,
  },
});

export default ContactList;
