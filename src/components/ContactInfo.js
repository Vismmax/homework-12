import React, {useEffect, useState} from 'react';
import {Avatar, Input, Icon, Button} from 'react-native-elements';
import {View, ScrollView, StyleSheet} from 'react-native';
import {phoneCall} from '../services/callService';
import {deleteContact, getContact} from '../services/contactService';

const ContactInfo = ({route, navigation}) => {
  const [contact, setContact] = useState({});

  const {displayName, thumbnailPath, phoneNumbers, emailAddresses} = contact;

  useEffect(() => {
    getContact(route.params.contact.id, (data) => {
      setContact(data);
    });
  }, [route]);

  const del = () => {
    deleteContact(contact, () => {
      navigation.navigate('Contacts', {action: 'delete'});
    });
  };

  const call = () => {
    const number = phoneNumbers[0]?.number;
    if (number) {
      phoneCall(number);
    }
  };

  return (
    <ScrollView style={styles.container}>
      <View>
        {thumbnailPath ? (
          <Avatar
            rounded
            size="xlarge"
            containerStyle={styles.avatar}
            source={thumbnailPath}
          />
        ) : (
          <Avatar
            rounded
            size="xlarge"
            containerStyle={styles.avatar}
            icon={{name: 'person', color: 'gray', size: 140}}
          />
        )}
      </View>
      <View>
        <Input
          label="Name"
          placeholder="Name"
          leftIcon={<Icon name="person" />}
          value={displayName}
          disabled
        />
        {phoneNumbers?.map(({label, number}) => (
          <Input
            key={number}
            label={`Phone number   (${label})`}
            placeholder="Phone number"
            leftIcon={<Icon name="call" />}
            value={number}
            disabled
          />
        ))}
        {emailAddresses?.map(({label, email}) => (
          <Input
            key={email}
            label={`Email   (${label})`}
            placeholder="Email"
            leftIcon={<Icon name="mail" />}
            value={email}
            disabled
          />
        ))}
      </View>
      <View style={styles.buttons}>
        <Button
          buttonStyle={styles.buttonCall}
          icon={<Icon name="call" />}
          title="Call"
          onPress={call}
        />
        <Button
          titleStyle={styles.buttonDel}
          icon={<Icon name="delete" />}
          type="clear"
          title="Delete"
          onPress={del}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  avatar: {
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: 'lightgrey',
  },
  buttons: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  buttonDel: {
    color: 'red',
  },
  buttonCall: {
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'green',
  },
});

export default ContactInfo;
