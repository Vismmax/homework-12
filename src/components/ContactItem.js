import React from 'react';
import {Icon, ListItem} from 'react-native-elements';

const ContactItem = ({contact, onPress, edit}) => {
  const {avatar, name, emails} = contact;
  return (
    <ListItem
      leftIcon={avatar ? false : {name: 'person'}}
      leftAvatar={
        avatar
          ? {
              source: {uri: avatar},
            }
          : false
      }
      title={name}
      subtitle={emails[0]}
      rightIcon={
        <Icon name="edit" color="darkgrey" onPress={() => edit(contact)} />
      }
      bottomDivider
      onPress={() => onPress(contact)}
    />
  );
};

export default ContactItem;
